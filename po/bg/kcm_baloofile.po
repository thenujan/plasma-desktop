# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Svetoslav Stefanov <svetlisashkov@yahoo.com>, 2014.
# Mincho Kondarev <mkondarev@yahoo.de>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: kcm_baloofile\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-08-10 00:47+0000\n"
"PO-Revision-Date: 2022-05-22 07:50+0200\n"
"Last-Translator: Mincho Kondarev <mkondarev@yahoo.de>\n"
"Language-Team: Bulgarian <kde-i18n-doc@kde.org>\n"
"Language: bg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.04.0\n"
"X-Poedit-SourceCharset: UTF-8\n"

#: package/contents/ui/main.qml:21
#, kde-format
msgid ""
"This module lets you configure the file indexer and search functionality."
msgstr ""
"Този модул ви позволява да конфигурирате файловия индексатор и "
"функционалността за търсене."

#: package/contents/ui/main.qml:52
#, kde-format
msgid ""
"This will disable file searching in KRunner and launcher menus, and remove "
"extended metadata display from all KDE applications."
msgstr ""
"Това ще деактивира търсенето в Krunner и Launcher менюто и ще премахне "
"разширените метаданни от всички KDE приложения."

#: package/contents/ui/main.qml:61
#, kde-format
msgid ""
"Do you want to delete the saved index data? %1 of space will be freed, but "
"if indexing is re-enabled later, the entire index will have to be re-created "
"from scratch. This may take some time, depending on how many files you have."
msgstr ""
"Искате ли да изтриете данните за запаметените индекси? %1 от пространството "
"ще бъдат освободени, но ако индексирането бъде повторно активирано по-късно, "
"целият индекс ще трябва да бъде отново създаден от нулата. Това може да "
"отнеме известно време, в зависимост от това колко файлове имате."

#: package/contents/ui/main.qml:63
#, kde-format
msgid "Delete Index Data"
msgstr "Изтриване на индексираните данни"

#: package/contents/ui/main.qml:73
#, kde-format
msgid ""
"File Search helps you quickly locate all your files based on their content."
msgstr ""
"Търсене на файлове ви помага бързо да намерите всичките си файлове въз "
"основа на тяхното съдържание."

#: package/contents/ui/main.qml:80
#, kde-format
msgid "Enable File Search"
msgstr "Активиране на търсенето на файлове"

#: package/contents/ui/main.qml:95
#, kde-format
msgid "Also index file content"
msgstr "Индексиране също и на съдържанието на файла"

#: package/contents/ui/main.qml:109
#, kde-format
msgid "Index hidden files and folders"
msgstr "Индексиране на скрити файлове и папки"

#: package/contents/ui/main.qml:133
#, kde-format
msgid "Status: %1, %2% complete"
msgstr "Статус: %1, %2% завършено"

#: package/contents/ui/main.qml:138
#, kde-format
msgid "Pause Indexer"
msgstr "Паузиране на индексирането"

#: package/contents/ui/main.qml:138
#, kde-format
msgid "Resume Indexer"
msgstr "Възобновяване на индексирането"

#: package/contents/ui/main.qml:151
#, kde-format
msgid "Currently indexing: %1"
msgstr "Текущо индексиране: %1"

#: package/contents/ui/main.qml:156
#, kde-format
msgid "Folder specific configuration:"
msgstr "Конфигурация, специфична за папка:"

#: package/contents/ui/main.qml:186
#, kde-format
msgid "Start indexing a folder…"
msgstr "Стартиране индексиране на папка…"

#: package/contents/ui/main.qml:197
#, kde-format
msgid "Stop indexing a folder…"
msgstr "Спиране индексиране на папка…"

#: package/contents/ui/main.qml:248
#, kde-format
msgid "Not indexed"
msgstr "Не е индексирано"

#: package/contents/ui/main.qml:249
#, kde-format
msgid "Indexed"
msgstr "Индексирано"

#: package/contents/ui/main.qml:279
#, kde-format
msgid "Delete entry"
msgstr "Изтриване на записа"

#: package/contents/ui/main.qml:294
#, kde-format
msgid "Select a folder to include"
msgstr "Избиране на папка за включване"

#: package/contents/ui/main.qml:294
#, kde-format
msgid "Select a folder to exclude"
msgstr "Избиране на папка за изключване"
