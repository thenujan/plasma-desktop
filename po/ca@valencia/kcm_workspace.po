# Translation of kcm_workspace.po to Catalan (Valencian)
# Copyright (C) 2014-2022 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Josep M. Ferrer <txemaq@gmail.com>, 2014, 2017, 2018, 2019, 2020, 2021, 2022.
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2020.
# Empar Montoro Martín <montoro_mde@gva.es>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-11-06 00:48+0000\n"
"PO-Revision-Date: 2022-06-20 19:29+0200\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca@valencia\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Generator: Lokalize 20.12.0\n"

#: package/contents/ui/main.qml:25
#, kde-format
msgid "Visual behavior:"
msgstr "Comportament visual:"

#. i18n: ectx: label, entry (delay), group (PlasmaToolTips)
#: package/contents/ui/main.qml:26 workspaceoptions_plasmasettings.kcfg:9
#, kde-format
msgid "Display informational tooltips on mouse hover"
msgstr "Mostra els consells informatius en passar-hi per damunt amb el ratolí"

#. i18n: ectx: label, entry (osdEnabled), group (OSD)
#: package/contents/ui/main.qml:37 workspaceoptions_plasmasettings.kcfg:15
#, kde-format
msgid "Display visual feedback for status changes"
msgstr "Mostra retroalimentació visual per als canvis d'estat"

#: package/contents/ui/main.qml:55
#, kde-format
msgid "Animation speed:"
msgstr "Velocitat de l'animació:"

#: package/contents/ui/main.qml:78
#, kde-format
msgctxt "Animation speed"
msgid "Slow"
msgstr "Lenta"

#: package/contents/ui/main.qml:84
#, kde-format
msgctxt "Animation speed"
msgid "Instant"
msgstr "Instantània"

#: package/contents/ui/main.qml:99
#, kde-format
msgctxt ""
"part of a sentence: 'Clicking files or folders [opens them/selects them]'"
msgid "Clicking files or folders:"
msgstr "En fer clic damunt de fitxers o carpetes:"

#: package/contents/ui/main.qml:100
#, kde-format
msgctxt "part of a sentence: 'Clicking files or folders opens them'"
msgid "Opens them"
msgstr "S'òbriguen"

#: package/contents/ui/main.qml:112
#, kde-format
msgid "Select by clicking on item's selection marker"
msgstr "Selecciona clicant en el marcador de selecció de l'element"

#: package/contents/ui/main.qml:122
#, kde-format
msgctxt "part of a sentence: 'Clicking files or folders selects them'"
msgid "Selects them"
msgstr "Se seleccionen"

#: package/contents/ui/main.qml:135
#, kde-format
msgid "Open by double-clicking instead"
msgstr "Obri fent doble clic"

#: package/contents/ui/main.qml:150
#, kde-format
msgid "Clicking in scrollbar track:"
msgstr "Fent clic en la via de la barra de desplaçament:"

#: package/contents/ui/main.qml:151
#, kde-format
msgctxt ""
"@radio part of a complete sentence: 'Clicking in scrollbar track scrolls one "
"page up or down'"
msgid "Scrolls one page up or down"
msgstr "Desplaça una pàgina amunt o avall"

#: package/contents/ui/main.qml:163
#, kde-format
msgid "Middle-click to scroll to clicked location"
msgstr "Clic del mig per a desplaçar cap a la ubicació clicada"

#: package/contents/ui/main.qml:173
#, kde-format
msgctxt ""
"@radio part of a complete sentence: 'Clicking in scrollbar track scrolls to "
"the clicked location'"
msgid "Scrolls to the clicked location"
msgstr "Desplaça cap a la ubicació clicada"

#: package/contents/ui/main.qml:192
#, kde-format
msgid "Middle Click:"
msgstr "Clic del mig:"

#: package/contents/ui/main.qml:194
#, kde-format
msgid "Paste selected text"
msgstr "Apega el text seleccionat"

#: package/contents/ui/main.qml:211
#, kde-format
msgid "Touch Mode:"
msgstr "Mode tàctil:"

#: package/contents/ui/main.qml:213
#, kde-format
msgctxt "As in: 'Touch Mode is automatically enabled as needed'"
msgid "Automatically enable as needed"
msgstr "Activa automàticament quan calga"

#: package/contents/ui/main.qml:213 package/contents/ui/main.qml:242
#, kde-format
msgctxt "As in: 'Touch Mode is never enabled'"
msgid "Never enabled"
msgstr "Mai activat"

#: package/contents/ui/main.qml:225
#, kde-format
msgid ""
"Touch Mode will be automatically activated whenever the system detects a "
"touchscreen but no mouse or touchpad. For example: when a transformable "
"laptop's keyboard is flipped around or detached."
msgstr ""
"El mode tàctil s'activarà automàticament quan el sistema detecte una "
"pantalla tàctil però sense ratolí ni ratolí tàctil. Per exemple: quan el "
"teclat d'un ordinador portàtil transformable es gire o se separe."

#: package/contents/ui/main.qml:230
#, kde-format
msgctxt "As in: 'Touch Mode is always enabled'"
msgid "Always enabled"
msgstr "Sempre activat"

#: package/contents/ui/main.qml:255
#, kde-format
msgid ""
"In Touch Mode, many elements of the user interface will become larger to "
"more easily accommodate touch interaction."
msgstr ""
"En el mode tàctil, molts elements de la interfície d'usuari es faran més "
"grans per a adaptar-se amb més facilitat a la interacció tàctil."

#. i18n: ectx: label, entry (singleClick), group (KDE)
#: workspaceoptions_kdeglobalssettings.kcfg:9
#, kde-format
msgid "Single click to open files"
msgstr "Clic únic per a obrir fitxers"

#. i18n: ectx: label, entry (animationDurationFactor), group (KDE)
#: workspaceoptions_kdeglobalssettings.kcfg:13
#, kde-format
msgid "Animation speed"
msgstr "Velocitat de l'animació"

#. i18n: ectx: label, entry (scrollbarLeftClickNavigatesByPage), group (KDE)
#: workspaceoptions_kdeglobalssettings.kcfg:17
#, kde-format
msgid "Left-click in scrollbar track moves scrollbar by one page"
msgstr "Fer clic esquerre en la barra de desplaçament desplaça una pàgina"

#. i18n: ectx: label, entry (tabletMode), group (Input)
#: workspaceoptions_kwinsettings.kcfg:9
#, kde-format
msgid "Automatically switch to touch-optimized mode"
msgstr "Canvia automàticament al mode optimitzat per als tocs"

#. i18n: ectx: label, entry (primarySelection), group (Wayland)
#: workspaceoptions_kwinsettings.kcfg:15
#, kde-format
msgid "Enable middle click selection pasting"
msgstr "Activa l'apegades de la selecció amb el clic del mig"

#. i18n: ectx: tooltip, entry (osdEnabled), group (OSD)
#: workspaceoptions_plasmasettings.kcfg:16
#, kde-format
msgid ""
"Show an on-screen display to indicate status changes such as brightness or "
"volume"
msgstr ""
"Mostra un missatge en pantalla per a indicar canvis d'estat com la "
"lluminositat o el volum"

#. i18n: ectx: label, entry (osdKbdLayoutChangedEnabled), group (OSD)
#: workspaceoptions_plasmasettings.kcfg:20
#, kde-format
msgid "OSD on layout change"
msgstr "OSD en canvi de disposició"

#. i18n: ectx: tooltip, entry (osdKbdLayoutChangedEnabled), group (OSD)
#: workspaceoptions_plasmasettings.kcfg:21
#, kde-format
msgid "Show a popup on layout changes"
msgstr "Mostra un missatge emergent en canvis de disposició"
