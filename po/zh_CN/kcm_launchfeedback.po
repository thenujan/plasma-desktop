msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-13 00:47+0000\n"
"PO-Revision-Date: 2022-12-01 06:53\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/plasma-desktop/kcm_launchfeedback.pot\n"
"X-Crowdin-File-ID: 7175\n"

#. i18n: ectx: label, entry (cursorTimeout), group (BusyCursorSettings)
#. i18n: ectx: label, entry (taskbarTimeout), group (TaskbarButtonSettings)
#: launchfeedbacksettingsbase.kcfg:15 launchfeedbacksettingsbase.kcfg:29
#, kde-format
msgid "Timeout in seconds"
msgstr "超时秒数"

#: package/contents/ui/main.qml:17
#, kde-format
msgid "Launch Feedback"
msgstr "程序启动动效"

#: package/contents/ui/main.qml:33
#, kde-format
msgid "Cursor:"
msgstr "光标动效："

#: package/contents/ui/main.qml:34
#, kde-format
msgid "No Feedback"
msgstr "无动效"

#: package/contents/ui/main.qml:49
#, kde-format
msgid "Static"
msgstr "静态"

#: package/contents/ui/main.qml:64
#, kde-format
msgid "Blinking"
msgstr "闪烁"

#: package/contents/ui/main.qml:79
#, kde-format
msgid "Bouncing"
msgstr "跳动"

#: package/contents/ui/main.qml:97
#, kde-format
msgid "Task Manager:"
msgstr "任务管理器："

#: package/contents/ui/main.qml:99
#, kde-format
msgid "Enable animation"
msgstr "启用动效"

#: package/contents/ui/main.qml:113
#, kde-format
msgid "Stop animation after:"
msgstr "动效持续时间："

#: package/contents/ui/main.qml:126 package/contents/ui/main.qml:138
#, kde-format
msgid "%1 second"
msgid_plural "%1 seconds"
msgstr[0] "%1 秒"
