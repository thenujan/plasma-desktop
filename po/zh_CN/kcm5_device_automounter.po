msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-12-10 00:45+0000\n"
"PO-Revision-Date: 2022-12-01 06:53\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/plasma-desktop/kcm5_device_automounter."
"pot\n"
"X-Crowdin-File-ID: 4761\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "KDE 中国, Guo Yunhe, Tyson Tan"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "kde-china@kde.org, i@guoyunhe.me, tysontan@tysontan.com"

#: DeviceAutomounterKCM.cpp:38
#, kde-format
msgid "Device Automounter"
msgstr "设备自动挂载器"

#: DeviceAutomounterKCM.cpp:42
#, kde-format
msgid "(c) 2009 Trever Fischer, (c) 2015 Kai Uwe Broulik"
msgstr "(c) 2009 Trever Fischer, (c) 2015 Kai Uwe Broulik"

#: DeviceAutomounterKCM.cpp:43
#, kde-format
msgid "Trever Fischer"
msgstr "Trever Fischer"

#: DeviceAutomounterKCM.cpp:43
#, kde-format
msgid "Original Author"
msgstr "原作者"

#: DeviceAutomounterKCM.cpp:44
#, kde-format
msgid "Kai Uwe Broulik"
msgstr "Kai Uwe Broulik"

#: DeviceAutomounterKCM.cpp:44
#, kde-format
msgid "Plasma 5 Port"
msgstr "Plasma 5 移植"

#. i18n: ectx: property (whatsThis), widget (QTreeView, deviceView)
#: DeviceAutomounterKCM.ui:17
#, kde-format
msgid ""
"This list contains the storage devices known to the system. If \"Automount "
"on Login\" is checked, the device will be automatically mounted even though "
"\"Mount all removable media at login\" is unchecked. The same applies for "
"\"Automount on Attach\"."
msgstr ""
"此列表包含本机系统已知的存储设备。 设备的“登录时”选框被勾选后，它将在系统登录"
"到桌面时自动挂载，即使“登录时自动挂载所有可移动设备”选框未被勾选。此规则同样"
"适用于设备的“插入时”选框。"

#. i18n: ectx: property (whatsThis), widget (QPushButton, forgetDevice)
#: DeviceAutomounterKCM.ui:54
#, kde-format
msgid ""
"Clicking this button causes the selected devices to be 'forgotten'. This is "
"only useful if \"Only automatically mount removable media that has been "
"manually mounted before\" is checked. Once a device is forgotten and the "
"system is set to only automatically mount familiar devices, the device will "
"not be automatically mounted."
msgstr ""
"点击此按钮会“忘记”所选的设备。这只有在“仅自动挂载手动挂载过的可移动设备”选中"
"时有用。设备记录删除后，KDE 将只自动挂载曾经挂载过的设备，不会自动挂载此设"
"备。"

#. i18n: ectx: property (text), widget (QPushButton, forgetDevice)
#: DeviceAutomounterKCM.ui:57
#, kde-format
msgid "Forget Device"
msgstr "忘记设备"

#. i18n: ectx: property (whatsThis), widget (QCheckBox, kcfg_AutomountUnknownDevices)
#: DeviceAutomounterKCM.ui:67
#, kde-format
msgid ""
"When this is not checked, only remembered devices will be automatically "
"mounted. A device is 'remembered' if it has ever been mounted before. For "
"instance, plugging in a USB media player to charge is not sufficient to "
"'remember' it - if the files are not accessed, it will not be automatically "
"mounted the next time it is seen. Once they have been accessed, however, the "
"device's contents will be automatically made available to the system."
msgstr ""
"不勾选此项时，只有记录的设备将被挂载。“记录”的设备是指曾被挂载过的设备，例"
"如，仅插入 USB 媒体播放器进行充电而不访问文件，不会被记录。在下次遇到该播放器"
"时不会自动挂载它。只要访问过，设备将被自动挂载以使系统能访问其内容。"

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_AutomountUnknownDevices)
#: DeviceAutomounterKCM.ui:70
#, kde-format
msgid "Automatically mount removable media that have never been mounted before"
msgstr "自动挂载新的可移动设备"

#: DeviceModel.cpp:52
#, kde-format
msgid "Automount Device"
msgstr "自动挂载设备"

#: DeviceModel.cpp:54
#, kde-format
msgctxt "As in automount on login"
msgid "On Login"
msgstr "登录时"

#: DeviceModel.cpp:56
#, kde-format
msgctxt "As in automount on attach"
msgid "On Attach"
msgstr "插入时"

#: DeviceModel.cpp:258
#, kde-format
msgid "All Devices"
msgstr "所有设备"

#: DeviceModel.cpp:258
#, kde-format
msgid "All Known Devices"
msgstr "所有已知设备"

#: DeviceModel.cpp:260
#, kde-format
msgid "Attached Devices"
msgstr "已插入设备"

#: DeviceModel.cpp:262
#, kde-format
msgid "Disconnected Devices"
msgstr "已拔出设备"

#: DeviceModel.cpp:298 DeviceModel.cpp:307
#, kde-format
msgid "UDI: %1"
msgstr "通用设备标识符：%1"

#: DeviceModel.cpp:318
#, kde-format
msgid "This device will be automatically mounted at login."
msgstr "登录时自动挂载此设备。"

#: DeviceModel.cpp:318
#, kde-format
msgid "This device will not be automatically mounted at login."
msgstr "登录时不自动挂载此设备。"

#: DeviceModel.cpp:326
#, kde-format
msgid "This device will be automatically mounted when attached."
msgstr "设备插入时自动挂载。"

#: DeviceModel.cpp:327
#, kde-format
msgid "This device will not be automatically mounted when attached."
msgstr "设备插入时不自动挂载。"
