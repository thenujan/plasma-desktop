# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2020, 2021.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-12-01 00:48+0000\n"
"PO-Revision-Date: 2021-10-05 13:23+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.08.1\n"

#: globalaccelmodel.cpp:113
#, kde-format
msgid "Applications"
msgstr "Toepassingen"

#: globalaccelmodel.cpp:113 globalaccelmodel.cpp:274
#, kde-format
msgid "System Services"
msgstr "Systeemservices"

#: globalaccelmodel.cpp:187
#, kde-format
msgctxt ""
"%1 is the name of the component, %2 is the action for which saving failed"
msgid "Error while saving shortcut %1: %2"
msgstr "Fout bij opslaan van sneltoets %1: %2"

#: globalaccelmodel.cpp:295
#, kde-format
msgctxt "%1 is the name of an application"
msgid "Error while adding %1, it seems it has no actions."
msgstr "Fout tijdens toevoegen van %1, er lijken geen acties te zijn."

#: globalaccelmodel.cpp:338
#, kde-format
msgid "Error while communicating with the global shortcuts service"
msgstr "Fout bij communicatie met de globale sneltoetsservice"

#: kcm_keys.cpp:50
#, kde-format
msgid "Failed to communicate with global shortcuts daemon"
msgstr "Communiceren met de globale daemon voor sneltoetsen is mislukt"

#: kcm_keys.cpp:224 package/contents/ui/main.qml:129
#: standardshortcutsmodel.cpp:29
#, kde-format
msgid "Common Actions"
msgstr "Gemeenschappelijke acties"

#: kcm_keys.cpp:230
#, kde-format
msgctxt "%2 is the name of a category inside the 'Common Actions' section"
msgid ""
"Shortcut %1 is already assigned to the common %2 action '%3'.\n"
"Do you want to reassign it?"
msgstr ""
"Sneltoets %1 is al toegekend aan de algemene %2 actie '%3'.\n"
"Wilt u het opnieuw toekennen?"

#: kcm_keys.cpp:234
#, kde-format
msgid ""
"Shortcut %1 is already assigned to action '%2' of %3.\n"
"Do you want to reassign it?"
msgstr ""
"Sneltoets %1 is al toegekend aan de actie '%2' van %3.\n"
"Wilt u het opnieuw toekennen?"

#: kcm_keys.cpp:235
#, kde-format
msgctxt "@title:window"
msgid "Found conflict"
msgstr "Conflict gevonden"

#: package/contents/ui/main.qml:41
#, kde-format
msgid "Cannot export scheme while there are unsaved changes"
msgstr "Kan schema niet exporteren wanneer er niet opgeslagen wijzigingen zijn"

#: package/contents/ui/main.qml:53
#, kde-format
msgid ""
"Select the components below that should be included in the exported scheme"
msgstr ""
"De onderstaande componenten selecteren die meegenomen zouden moeten worden "
"in het geëxporteerde schema"

#: package/contents/ui/main.qml:59
#, kde-format
msgid "Save scheme"
msgstr "Schema opslaan"

#: package/contents/ui/main.qml:136
#, kde-format
msgid "Remove all shortcuts for %1"
msgstr "Alle sneltoetsen voor %1 verwijderen"

#: package/contents/ui/main.qml:147
#, kde-format
msgid "Undo deletion"
msgstr "Verwijdering ongedaan maken"

#: package/contents/ui/main.qml:198
#, kde-format
msgid "No items matched the search terms"
msgstr "Geen items komen overeen met de zoektermen"

#: package/contents/ui/main.qml:226
#, kde-format
msgid "Select an item from the list to view its shortcuts here"
msgstr "Een item uit de lijst selecteren om hier zijn sneltoets te bekijken"

#: package/contents/ui/main.qml:234
#, kde-format
msgid "Add Application…"
msgstr "Toepassing toevoegen…"

#: package/contents/ui/main.qml:244
#, kde-format
msgid "Import Scheme…"
msgstr "Schema importeren…"

#: package/contents/ui/main.qml:249
#, kde-format
msgid "Cancel Export"
msgstr "Exporteren annuleren"

#: package/contents/ui/main.qml:249
#, kde-format
msgid "Export Scheme…"
msgstr "Schema exporteren…"

#: package/contents/ui/main.qml:270
#, kde-format
msgid "Export Shortcut Scheme"
msgstr "Sneltoetsschema exporteren"

#: package/contents/ui/main.qml:270 package/contents/ui/main.qml:293
#, kde-format
msgid "Import Shortcut Scheme"
msgstr "Sneltoetsschema importeren"

#: package/contents/ui/main.qml:272
#, kde-format
msgctxt "Template for file dialog"
msgid "Shortcut Scheme (*.kksrc)"
msgstr "Sneltoetsschema (*.kksrc)"

#: package/contents/ui/main.qml:298
#, kde-format
msgid "Select the scheme to import:"
msgstr "Het te importeren schema selecteren:"

#: package/contents/ui/main.qml:310
#, kde-format
msgid "Custom Scheme"
msgstr "Aangepast schema"

#: package/contents/ui/main.qml:315
#, kde-format
msgid "Select File…"
msgstr "Bestand selecteren…"

#: package/contents/ui/main.qml:315
#, kde-format
msgid "Import"
msgstr "Importeren"

#: package/contents/ui/ShortcutActionDelegate.qml:27
#, kde-format
msgid "Editing shortcut: %1"
msgstr "Sneltoets wordt bewerkt: %1"

#: package/contents/ui/ShortcutActionDelegate.qml:39
#, kde-format
msgctxt ""
"%1 is the name action that is triggered by the key sequences following "
"after :"
msgid "%1:"
msgstr "%1:"

#: package/contents/ui/ShortcutActionDelegate.qml:52
#, kde-format
msgid "No active shortcuts"
msgstr "Geen actieve sneltoetsen"

#: package/contents/ui/ShortcutActionDelegate.qml:92
#, kde-format
msgctxt "%1 decides if singular or plural will be used"
msgid "Default shortcut"
msgid_plural "Default shortcuts"
msgstr[0] "Standaard sneltoets"
msgstr[1] "Standaard sneltoetsen"

#: package/contents/ui/ShortcutActionDelegate.qml:94
#, kde-format
msgid "No default shortcuts"
msgstr "Geen standaard sneltoetsen"

#: package/contents/ui/ShortcutActionDelegate.qml:102
#, kde-format
msgid "Default shortcut %1 is enabled."
msgstr "Standaard sneltoets %1 is ingeschakeld"

#: package/contents/ui/ShortcutActionDelegate.qml:102
#, kde-format
msgid "Default shortcut %1 is disabled."
msgstr "Standaard sneltoets %1 is uitgeschakeld"

#: package/contents/ui/ShortcutActionDelegate.qml:123
#, kde-format
msgid "Custom shortcuts"
msgstr "Aangepaste sneltoetsen"

#: package/contents/ui/ShortcutActionDelegate.qml:147
#, kde-format
msgid "Delete this shortcut"
msgstr "Deze sneltoets verwijderen"

#: package/contents/ui/ShortcutActionDelegate.qml:153
#, kde-format
msgid "Add custom shortcut"
msgstr "Aangepaste sneltoets toevoegen"

#: package/contents/ui/ShortcutActionDelegate.qml:187
#, kde-format
msgid "Cancel capturing of new shortcut"
msgstr "Vangen van nieuwe sneltoets annuleren"

#: standardshortcutsmodel.cpp:33
#, kde-format
msgid "File"
msgstr "Bestand"

#: standardshortcutsmodel.cpp:34
#, kde-format
msgid "Edit"
msgstr "Bewerken"

#: standardshortcutsmodel.cpp:36
#, kde-format
msgid "Navigation"
msgstr "Navigatie"

#: standardshortcutsmodel.cpp:37
#, kde-format
msgid "View"
msgstr "Beeld"

#: standardshortcutsmodel.cpp:38
#, kde-format
msgid "Settings"
msgstr "Instellingen"

#: standardshortcutsmodel.cpp:39
#, kde-format
msgid "Help"
msgstr "Help"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Freek de Kruijf - 2020 t/m 2021"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "freekdekruijf@kde.nl"

#~ msgid "Shortcuts"
#~ msgstr "Sneltoetsen"

#~ msgid "David Redondo"
#~ msgstr "David Redondo"
