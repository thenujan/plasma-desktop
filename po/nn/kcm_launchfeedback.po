# Translation of kcm_launchfeedback to Norwegian Nynorsk
#
# Gaute Hvoslef Kvalnes <gaute@verdsveven.com>, 2001, 2002, 2004.
# Karl Ove Hufthammer <karl@huftis.org>, 2007, 2008, 2018, 2020, 2022.
msgid ""
msgstr ""
"Project-Id-Version: kcmlaunch\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-13 00:47+0000\n"
"PO-Revision-Date: 2022-01-15 18:12+0100\n"
"Last-Translator: Karl Ove Hufthammer <karl@huftis.org>\n"
"Language-Team: Norwegian Nynorsk <l10n-no@lister.huftis.org>\n"
"Language: nn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 21.12.1\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#. i18n: ectx: label, entry (cursorTimeout), group (BusyCursorSettings)
#. i18n: ectx: label, entry (taskbarTimeout), group (TaskbarButtonSettings)
#: launchfeedbacksettingsbase.kcfg:15 launchfeedbacksettingsbase.kcfg:29
#, kde-format
msgid "Timeout in seconds"
msgstr "Tidsgrense (sekund)"

#: package/contents/ui/main.qml:17
#, kde-format
msgid "Launch Feedback"
msgstr "Signal ved programstart"

#: package/contents/ui/main.qml:33
#, kde-format
msgid "Cursor:"
msgstr "Peikar:"

#: package/contents/ui/main.qml:34
#, kde-format
msgid "No Feedback"
msgstr "Inga tilbakemelding"

#: package/contents/ui/main.qml:49
#, kde-format
msgid "Static"
msgstr "Statisk"

#: package/contents/ui/main.qml:64
#, kde-format
msgid "Blinking"
msgstr "Blinkande"

#: package/contents/ui/main.qml:79
#, kde-format
msgid "Bouncing"
msgstr "Sprettande"

#: package/contents/ui/main.qml:97
#, kde-format
msgid "Task Manager:"
msgstr "Oppgåvehandsamar:"

#: package/contents/ui/main.qml:99
#, kde-format
msgid "Enable animation"
msgstr "Slå på animasjonar"

#: package/contents/ui/main.qml:113
#, kde-format
msgid "Stop animation after:"
msgstr "Stopp animasjonar etter:"

#: package/contents/ui/main.qml:126 package/contents/ui/main.qml:138
#, kde-format
msgid "%1 second"
msgid_plural "%1 seconds"
msgstr[0] "%1 sekund"
msgstr[1] "%1 sekund"
