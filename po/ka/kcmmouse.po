# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-20 00:51+0000\n"
"PO-Revision-Date: 2022-12-12 09:43+0100\n"
"Last-Translator: Temuri Doghonadze <temuri.doghonadze@gmail.com>\n"
"Language-Team: Georgian <kde-i18n-doc@kde.org>\n"
"Language: ka\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.2.2\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Temuri Doghonadze"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "Temuri.doghonadze@gmail.com"

#: backends/kwin_wl/kwin_wl_backend.cpp:66
#, kde-format
msgid "Querying input devices failed. Please reopen this settings module."
msgstr ""
"შეყვანის მოწყობილობის ინფორმაციის მოთხოვნის შეცდომა. ეს მოდული თავიდან "
"გახსენით."

#: backends/kwin_wl/kwin_wl_backend.cpp:86
#, kde-format
msgid "Critical error on reading fundamental device infos of %1."
msgstr ""
"კრიტიკული შეცდომა მოწყობილობის ფუნდამენტალური ინფორმაციის მიღებისას: %1."

#: kcm/libinput/libinput_config.cpp:36
#, kde-format
msgid "Pointer device KCM"
msgstr "თაგუნას KCM"

#: kcm/libinput/libinput_config.cpp:38
#, kde-format
msgid "System Settings module for managing mice and trackballs."
msgstr ""
"სისტემური პარამეტრების მოდული თქვენი თაგუნების და ტრეკბოლების სამართავად."

#: kcm/libinput/libinput_config.cpp:40
#, kde-format
msgid "Copyright 2018 Roman Gilg"
msgstr "Copyright 2018 Roman Gilg"

#: kcm/libinput/libinput_config.cpp:43 kcm/xlib/xlib_config.cpp:91
#, kde-format
msgid "Roman Gilg"
msgstr "Roman Gilg"

#: kcm/libinput/libinput_config.cpp:43
#, kde-format
msgid "Developer"
msgstr "პროგრამისტი"

#: kcm/libinput/libinput_config.cpp:109
#, kde-format
msgid ""
"Error while loading values. See logs for more information. Please restart "
"this configuration module."
msgstr ""
"მნიშვნელობების ჩატვირთვის შეცდომა. მეტი ინფორმაციის მისაღებად იხილეთ "
"ჟურნალი. საჭიროა ამ კონფიგურაციის მოდულის გადატვირთვა."

#: kcm/libinput/libinput_config.cpp:114
#, kde-format
msgid "No pointer device found. Connect now."
msgstr "თაგუნა ვერ ვიპოვე. შეაერთეთ."

#: kcm/libinput/libinput_config.cpp:125
#, kde-format
msgid ""
"Not able to save all changes. See logs for more information. Please restart "
"this configuration module and try again."
msgstr ""

#: kcm/libinput/libinput_config.cpp:145
#, kde-format
msgid ""
"Error while loading default values. Failed to set some options to their "
"default values."
msgstr ""
"ნაგულისხმები მნიშვნელობების ჩატვირთვის შეცდომა. ზოგიერთი პარამეტრის მათ "
"ნაგულისხმებ მნიშვნელობებზე დაყენების შეცდომა."

#: kcm/libinput/libinput_config.cpp:167
#, kde-format
msgid ""
"Error while adding newly connected device. Please reconnect it and restart "
"this configuration module."
msgstr ""
"შეცდომა ახლად შეერთებული მოწყობილობის დამატებისას. თავიდან მიაერთეთ ის და ეს "
"მოდული თავიდან გაუშვით."

#: kcm/libinput/libinput_config.cpp:191
#, kde-format
msgid "Pointer device disconnected. Closed its setting dialog."
msgstr "თაგუნა გაითიშა. მისი პარამეტრების ფანჯარა დაიხურა."

#: kcm/libinput/libinput_config.cpp:193
#, kde-format
msgid "Pointer device disconnected. No other devices found."
msgstr "თაგუნა გაითიშა. სხვა მოწყობილობა ნაპოვნი არაა."

#: kcm/libinput/main.qml:79
#, kde-format
msgid "Device:"
msgstr "მოწყობილობა:"

#: kcm/libinput/main.qml:103 kcm/libinput/main_deviceless.qml:54
#, kde-format
msgid "General:"
msgstr "საერთო:"

#: kcm/libinput/main.qml:105
#, kde-format
msgid "Device enabled"
msgstr "მოწყობილობა ჩართულია"

#: kcm/libinput/main.qml:124
#, kde-format
msgid "Accept input through this device."
msgstr "ამ მოწყობილობიდან შემოტანის ჩართვა."

#: kcm/libinput/main.qml:130 kcm/libinput/main_deviceless.qml:56
#, kde-format
msgid "Left handed mode"
msgstr "ცაცია"

#: kcm/libinput/main.qml:149 kcm/libinput/main_deviceless.qml:75
#, kde-format
msgid "Swap left and right buttons."
msgstr "მარჯვენა და მარცხენა ღილაკებისთვის ადგილის შეცვლა."

#: kcm/libinput/main.qml:155 kcm/libinput/main_deviceless.qml:81
#, kde-format
msgid "Press left and right buttons for middle-click"
msgstr "შუა-წკაპისთვის მარჯვენა და მარცხენა ღილაკებს ერთდროულად დააწექით"

#: kcm/libinput/main.qml:174 kcm/libinput/main_deviceless.qml:100
#, kde-format
msgid ""
"Clicking left and right button simultaneously sends middle button click."
msgstr ""
"მარცხენა და მარჯვენა ღილაკებზე ერთდროული წკაპი შუა ღილაკის წკაპს აგზავნის."

#: kcm/libinput/main.qml:184 kcm/libinput/main_deviceless.qml:110
#, kde-format
msgid "Pointer speed:"
msgstr "კურსორის სიჩქარე:"

#: kcm/libinput/main.qml:216 kcm/libinput/main_deviceless.qml:142
#, kde-format
msgid "Acceleration profile:"
msgstr "აჩქარების პროფილი:"

#: kcm/libinput/main.qml:247 kcm/libinput/main_deviceless.qml:173
#, kde-format
msgid "Flat"
msgstr "ბრტყელი"

#: kcm/libinput/main.qml:250 kcm/libinput/main_deviceless.qml:176
#, kde-format
msgid "Cursor moves the same distance as the mouse movement."
msgstr "კურსორი იგივე მანძილით გაიწევა, როგორითაც თაგუნა."

#: kcm/libinput/main.qml:257 kcm/libinput/main_deviceless.qml:183
#, kde-format
msgid "Adaptive"
msgstr "შეგუებადი"

#: kcm/libinput/main.qml:260 kcm/libinput/main_deviceless.qml:186
#, kde-format
msgid "Cursor travel distance depends on the mouse movement speed."
msgstr ""

#: kcm/libinput/main.qml:272 kcm/libinput/main_deviceless.qml:198
#, kde-format
msgid "Scrolling:"
msgstr "აწევ-ჩამოწევა:"

#: kcm/libinput/main.qml:274 kcm/libinput/main_deviceless.qml:200
#, kde-format
msgid "Invert scroll direction"
msgstr "აწევ-ჩამოწევის მიმართულების ინვერსია (ჩვეულებრივი აწევ-ჩამოწევა)"

#: kcm/libinput/main.qml:289 kcm/libinput/main_deviceless.qml:215
#, kde-format
msgid "Touchscreen like scrolling."
msgstr "სენსორული ეკრანის მსგავსი აწევ-ჩამოწევა."

#: kcm/libinput/main.qml:295
#, kde-format
msgid "Scrolling speed:"
msgstr "აწევ-ჩამოწევის სიჩქარე:"

#: kcm/libinput/main.qml:343
#, kde-format
msgctxt "Slower Scroll"
msgid "Slower"
msgstr "უფრო ნელა"

#: kcm/libinput/main.qml:349
#, kde-format
msgctxt "Faster Scroll Speed"
msgid "Faster"
msgstr "უფრო სწრაფი"

#: kcm/libinput/main.qml:360
#, kde-format
msgctxt "@action:button"
msgid "Re-bind Additional Mouse Buttons…"
msgstr ""

#: kcm/libinput/main.qml:396
#, kde-format
msgctxt "@label for assigning an action to a numbered button"
msgid "Extra Button %1:"
msgstr "დამატებითი ღილაკი %1:"

#: kcm/libinput/main.qml:426
#, kde-format
msgctxt "@action:button"
msgid "Press the mouse button for which you want to add a key binding"
msgstr ""

#: kcm/libinput/main.qml:427
#, kde-format
msgctxt "@action:button, %1 is the translation of 'Extra Button %1' from above"
msgid "Enter the new key combination for %1"
msgstr "შეიყვანეთ ახალი ღილაკების კომბინაცია %1-სთვის"

#: kcm/libinput/main.qml:431
#, kde-format
msgctxt "@action:button"
msgid "Cancel"
msgstr "შეწყვეტა"

#: kcm/libinput/main.qml:448
#, kde-format
msgctxt "@action:button"
msgid "Press a mouse button "
msgstr "დააწექით თაგუნას ღილაკს "

#: kcm/libinput/main.qml:449
#, kde-format
msgctxt "@action:button, Bind a mousebutton to keyboard key(s)"
msgid "Add Binding…"
msgstr "მიბმის დამატება…"

#: kcm/libinput/main.qml:478
#, kde-format
msgctxt "@action:button"
msgid "Go back"
msgstr "უკან გადასვლა"

#. i18n: ectx: property (whatsThis), widget (QWidget, KCMMouse)
#: kcm/xlib/kcmmouse.ui:14
#, kde-format
msgid ""
"<h1>Mouse</h1> This module allows you to choose various options for the way "
"in which your pointing device works. Your pointing device may be a mouse, "
"trackball, or some other hardware that performs a similar function."
msgstr ""

#. i18n: ectx: attribute (title), widget (QWidget, generalTab)
#: kcm/xlib/kcmmouse.ui:36
#, kde-format
msgid "&General"
msgstr "&ზოგადი"

#. i18n: ectx: property (whatsThis), widget (QGroupBox, handedBox)
#: kcm/xlib/kcmmouse.ui:52
#, kde-format
msgid ""
"If you are left-handed, you may prefer to swap the functions of the left and "
"right buttons on your pointing device by choosing the 'left-handed' option. "
"If your pointing device has more than two buttons, only those that function "
"as the left and right buttons are affected. For example, if you have a three-"
"button mouse, the middle button is unaffected."
msgstr ""

#. i18n: ectx: property (title), widget (QGroupBox, handedBox)
#: kcm/xlib/kcmmouse.ui:55
#, kde-format
msgid "Button Order"
msgstr "ღილაკების მიმდევრობა"

#. i18n: ectx: property (text), widget (QRadioButton, rightHanded)
#: kcm/xlib/kcmmouse.ui:64
#, kde-format
msgid "Righ&t handed"
msgstr "მარჯვენახელიანი"

#. i18n: ectx: property (text), widget (QRadioButton, leftHanded)
#: kcm/xlib/kcmmouse.ui:77
#, kde-format
msgid "Le&ft handed"
msgstr "ცაცია"

#. i18n: ectx: property (whatsThis), widget (QCheckBox, cbScrollPolarity)
#: kcm/xlib/kcmmouse.ui:106
#, kde-format
msgid ""
"Change the direction of scrolling for the mouse wheel or the 4th and 5th "
"mouse buttons."
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, cbScrollPolarity)
#: kcm/xlib/kcmmouse.ui:109
#, kde-format
msgid "Re&verse scroll direction"
msgstr "&აწევ-ჩამოწევის მიმართულების ინვერსია"

#. i18n: ectx: attribute (title), widget (QWidget, advancedTab)
#: kcm/xlib/kcmmouse.ui:156
#, kde-format
msgid "Advanced"
msgstr "დაწინაურებული"

#. i18n: ectx: property (text), widget (QLabel, label)
#: kcm/xlib/kcmmouse.ui:164
#, kde-format
msgid "Pointer acceleration:"
msgstr "კურსორის აჩქარება:"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: kcm/xlib/kcmmouse.ui:174
#, kde-format
msgid "Pointer threshold:"
msgstr "კურსორის ზღვარი:"

#. i18n: ectx: property (text), widget (QLabel, label_3)
#: kcm/xlib/kcmmouse.ui:184
#, kde-format
msgid "Double click interval:"
msgstr "ორაგი წკაპის ინტერვალი:"

#. i18n: ectx: property (text), widget (QLabel, label_4)
#: kcm/xlib/kcmmouse.ui:194
#, kde-format
msgid "Drag start time:"
msgstr "გადათრევის დაწყების დრო:"

#. i18n: ectx: property (text), widget (QLabel, label_5)
#: kcm/xlib/kcmmouse.ui:204
#, kde-format
msgid "Drag start distance:"
msgstr "გადათრევის დაწყების დაშორება:"

#. i18n: ectx: property (text), widget (QLabel, label_6)
#: kcm/xlib/kcmmouse.ui:214
#, kde-format
msgid "Mouse wheel scrolls by:"
msgstr "თაგუნას რგოლის აწევ-ჩამოწევის დონე:"

#. i18n: ectx: property (whatsThis), widget (QDoubleSpinBox, accel)
#: kcm/xlib/kcmmouse.ui:230
#, kde-format
msgid ""
"<p>This option allows you to change the relationship between the distance "
"that the mouse pointer moves on the screen and the relative movement of the "
"physical device itself (which may be a mouse, trackball, or some other "
"pointing device.)</p><p> A high value for the acceleration will lead to "
"large movements of the mouse pointer on the screen even when you only make a "
"small movement with the physical device. Selecting very high values may "
"result in the mouse pointer flying across the screen, making it hard to "
"control.</p>"
msgstr ""

#. i18n: ectx: property (suffix), widget (QDoubleSpinBox, accel)
#: kcm/xlib/kcmmouse.ui:233
#, kde-format
msgid " x"
msgstr " x"

#. i18n: ectx: property (whatsThis), widget (QSpinBox, thresh)
#: kcm/xlib/kcmmouse.ui:261
#, kde-format
msgid ""
"<p>The threshold is the smallest distance that the mouse pointer must move "
"on the screen before acceleration has any effect. If the movement is smaller "
"than the threshold, the mouse pointer moves as if the acceleration was set "
"to 1X;</p><p> thus, when you make small movements with the physical device, "
"there is no acceleration at all, giving you a greater degree of control over "
"the mouse pointer. With larger movements of the physical device, you can "
"move the mouse pointer rapidly to different areas on the screen.</p>"
msgstr ""

#. i18n: ectx: property (whatsThis), widget (QSpinBox, doubleClickInterval)
#: kcm/xlib/kcmmouse.ui:280
#, kde-format
msgid ""
"The double click interval is the maximal time (in milliseconds) between two "
"mouse clicks which turns them into a double click. If the second click "
"happens later than this time interval after the first click, they are "
"recognized as two separate clicks."
msgstr ""

#. i18n: ectx: property (suffix), widget (QSpinBox, doubleClickInterval)
#. i18n: ectx: property (suffix), widget (QSpinBox, dragStartTime)
#. i18n: ectx: property (suffix), widget (QSpinBox, mk_delay)
#. i18n: ectx: property (suffix), widget (QSpinBox, mk_interval)
#. i18n: ectx: property (suffix), widget (QSpinBox, mk_time_to_max)
#: kcm/xlib/kcmmouse.ui:283 kcm/xlib/kcmmouse.ui:311 kcm/xlib/kcmmouse.ui:408
#: kcm/xlib/kcmmouse.ui:450 kcm/xlib/kcmmouse.ui:482
#, kde-format
msgid " msec"
msgstr " მწმ"

#. i18n: ectx: property (whatsThis), widget (QSpinBox, dragStartTime)
#: kcm/xlib/kcmmouse.ui:308
#, kde-format
msgid ""
"If you click with the mouse (e.g. in a multi-line editor) and begin to move "
"the mouse within the drag start time, a drag operation will be initiated."
msgstr ""

#. i18n: ectx: property (whatsThis), widget (QSpinBox, dragStartDist)
#: kcm/xlib/kcmmouse.ui:333
#, kde-format
msgid ""
"If you click with the mouse and begin to move the mouse at least the drag "
"start distance, a drag operation will be initiated."
msgstr ""

#. i18n: ectx: property (whatsThis), widget (QSpinBox, wheelScrollLines)
#: kcm/xlib/kcmmouse.ui:355
#, kde-format
msgid ""
"If you use the wheel of a mouse, this value determines the number of lines "
"to scroll for each wheel movement. Note that if this number exceeds the "
"number of visible lines, it will be ignored and the wheel movement will be "
"handled as a page up/down movement."
msgstr ""

#. i18n: ectx: attribute (title), widget (QWidget, MouseNavigation)
#: kcm/xlib/kcmmouse.ui:387
#, kde-format
msgid "Keyboard Navigation"
msgstr "კლავიატურით ნავიგაცია"

#. i18n: ectx: property (text), widget (QCheckBox, mouseKeys)
#: kcm/xlib/kcmmouse.ui:395
#, kde-format
msgid "&Move pointer with keyboard (using the num pad)"
msgstr "&თაგუნას კურსორის კლავიატურით გადაადგილება (numpad-ით)"

#. i18n: ectx: property (text), widget (QLabel, label_7)
#: kcm/xlib/kcmmouse.ui:424
#, kde-format
msgid "&Acceleration delay:"
msgstr "&აჩქარების დაყოვნება:"

#. i18n: ectx: property (text), widget (QLabel, label_8)
#: kcm/xlib/kcmmouse.ui:434
#, kde-format
msgid "R&epeat interval:"
msgstr "&გამეორების ინტერვალი:"

#. i18n: ectx: property (text), widget (QLabel, label_9)
#: kcm/xlib/kcmmouse.ui:466
#, kde-format
msgid "Acceleration &time:"
msgstr "&აჩქარების დრო:"

#. i18n: ectx: property (text), widget (QLabel, label_10)
#: kcm/xlib/kcmmouse.ui:498
#, kde-format
msgid "Ma&ximum speed:"
msgstr "მაქსიმალური სიჩქარე:"

#. i18n: ectx: property (suffix), widget (QSpinBox, mk_max_speed)
#: kcm/xlib/kcmmouse.ui:514
#, kde-format
msgid " pixel/sec"
msgstr " პიქსელ/წმ"

#. i18n: ectx: property (text), widget (QLabel, label_11)
#: kcm/xlib/kcmmouse.ui:530
#, kde-format
msgid "Acceleration &profile:"
msgstr "აჩქარების &პროფილი:"

#: kcm/xlib/xlib_config.cpp:79
#, kde-format
msgid "Mouse"
msgstr "თაგუნა"

#: kcm/xlib/xlib_config.cpp:83
#, kde-format
msgid "(c) 1997 - 2018 Mouse developers"
msgstr "(c) 1997 - 2018 Mouse developers"

#: kcm/xlib/xlib_config.cpp:84
#, kde-format
msgid "Patrick Dowler"
msgstr "Patrick Dowler"

#: kcm/xlib/xlib_config.cpp:85
#, kde-format
msgid "Dirk A. Mueller"
msgstr "Dirk A. Mueller"

#: kcm/xlib/xlib_config.cpp:86
#, kde-format
msgid "David Faure"
msgstr "David Faure"

#: kcm/xlib/xlib_config.cpp:87
#, kde-format
msgid "Bernd Gehrmann"
msgstr "Bernd Gehrmann"

#: kcm/xlib/xlib_config.cpp:88
#, kde-format
msgid "Rik Hemsley"
msgstr "Rik Hemsley"

#: kcm/xlib/xlib_config.cpp:89
#, kde-format
msgid "Brad Hughes"
msgstr "Brad Hughes"

#: kcm/xlib/xlib_config.cpp:90
#, kde-format
msgid "Brad Hards"
msgstr "Brad Hards"

#: kcm/xlib/xlib_config.cpp:283 kcm/xlib/xlib_config.cpp:288
#, kde-format
msgid " pixel"
msgid_plural " pixels"
msgstr[0] " პიქსელი"
msgstr[1] " პიქსელი"

#: kcm/xlib/xlib_config.cpp:293
#, kde-format
msgid " line"
msgid_plural " lines"
msgstr[0] " ხაზი"
msgstr[1] " ხაზი"
